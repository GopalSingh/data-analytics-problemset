import sys
import json

import math
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def create_horizontal_bar(basic_stats):
	fig, ax = plt.subplots()

	features = list(basic_stats)
	y_pos = np.arange(len(features))

	performance = [basic_stats[key]['mean'] for key in features]
	print(performance)

	error = np.random.rand((len(features)))

	ax.barh(y_pos, performance, xerr=error, align='center', color='green', ecolor='red')

	ax.set_yticks(y_pos)

	ax.set_yticklabels(features)

	ax.invert_yaxis()
	ax.set_xlabel('Performance')
	ax.set_title("Normal Activity Monitor")

	plt.show()


def visualize_standardization(np_array, z_scores_np):

	y_pos = [[0,0,0,0,0,0,0,0] for i in range(len(np_array))]
	axis = plt.subplot()

	axis.scatter(z_scores_np, y_pos, color='b')
	axis.set_title('NumPy features standardization', color='b')

	plt.tight_layout()

	axis.get_yaxis().set_visible(False)
	axis.grid()

	plt.show()



def main():
	# Make sure you have an up-to-date version of Python
	# noinspection PyPep8,PyPep8
	if sys.version_info <= (2,7):
		raise Exception('You don\'t have an up to date version of Python')

	# Load the CSV file as a Pandas dataframe
	dataframe = pd.read_csv('NzM1MjYwOWUxNTg4.csv')

	# Remove any unnecessary metadata
	# noinspection PyPep8,PyPep8
	dataframe = dataframe.drop([
		'patient_id', 'calories', 'human_id', 'source_created_at', 'source_data', 'source_updated_at', 'source','intraday'
	], axis=1, errors='ignore')
	
	# YOUR CODE GOES HERE

	dataframe.index = dataframe['date']

	#print(dataframe)

	plt.figure(); dataframe.plot(); plt.legend(loc='best');plt.title("Before standardization")

	#print(dataframe.corr())

	basic_stats = dataframe.describe()

	#print(basic_stats)

	create_horizontal_bar(basic_stats)

	#Standardization

	np_array = np.asarray(dataframe.drop(['date', 'intraday', 'source'], axis=1, errors='ignore'))

	#print ("***NP_ARRAY***")
	#print(np_array)

	z_scores_np = (np_array - np_array.mean()) / np_array.std()

	###print(z_scores_np)

	visualize_standardization(np_array, z_scores_np)

	features = list(basic_stats)

	df = dataframe.replace({key: {0: basic_stats[key]['mean']} for key in features})

	#print(df)

	plt.figure();
	df.plot();
	plt.legend(loc='best');
	plt.title("After standardization and replacement")
	plt.show()


# noinspection PyPep8
if __name__ == '__main__':
	# noinspection PyPep8
	main()